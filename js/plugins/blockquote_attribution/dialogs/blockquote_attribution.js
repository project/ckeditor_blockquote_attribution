(function() {
    function blockquoteAttributionDialog( editor, isEdit ) {
        return {
            title : Drupal.t('Blockquote Attribution Dialog'),
            minWidth : 500,
            minHeight : 50,
            contents : [
                {
                    id: 'info',
                    label: Drupal.t('Add a blockquote with source attribution'),
                    title: Drupal.t('Add a blockquote with source attribution'),
                    elements:
                        [
                            {
                                id: 'cite',
                                type: 'text',
                                label: Drupal.t('Source:'),
                                setup: function (element) {
                                    if (isEdit)
                                        this.setValue(element.getAttribute('cite'));
                                }
                            },
                        ],
                }
            ],
            //onShow : function() {
            //    this.setupContent();
            //},
            onOk : function() {
                var cite = this.getValueOf('info', 'cite');

                CKEDITOR.plugins.blockquote_attribution.createBlockquoteAttribution( editor, cite);
            }
        }
    }

    CKEDITOR.dialog.add( 'create_blockquote_attribution', function( editor ) {
        return blockquoteAttributionDialog( editor );
    });

})();
